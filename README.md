## **Resource Links:**
- [Useful Links](http://www.qcitr.com/usefullinks.htm)
- [DB Links](http://www.qcitr.com/dblinks.htm)

===================================

## **Preliminary Requirements:**
## 1. **Install:**


+ [Download MySQL](http://dev.mysql.com/downloads/mysql/ "MySQL Download")
    - Windows Users: Get no-install (ZIP Archive): either 32- or 64-bit versions, depending upon your OS.
    - Mac Users: Get your OS version DMG Archive


## 2. **Configure MySQL:**
+ Configure MySQL:
    - [MySQL Set Up](https://bitbucket.org/mjowett/lis3781/raw/master/docs/MySQL_Installation_Admin_Brief.pdf "MySQL Installation")
    - [Mac Users](https://dev.mysql.com/doc/refman/5.6/en/osx-installation-pkg.html "MySQL Macs")

===================================

### TODO:	
1. Install MySQL
2. Configure MySQL
3. [Granting privileges](https://bitbucket.org/mjowett/lis3781/raw/master/docs/MySQL_Installation_Admin_Brief.pdf "Granting privileges")
4. [Set users/roles/permissions](https://bitbucket.org/mjowett/lis3781/raw/master/docs/lis3781_permissions.doc "Set user permissions")
5. [Enforcing_PK_FK_Relationship.pdf](https://bitbucket.org/mjowett/lis3781/raw/master/docs/Enforcing_PK_FK_Relationship.pdf "pk/fk relationships")
6. [create_region_store.sql](https://bitbucket.org/mjowett/lis3781/raw/master/docs/create_region_store.sql "create tables example")
7. tee/notee


#### SQL Example:
```sql
select * from tablename;
```
